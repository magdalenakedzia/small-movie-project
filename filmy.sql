-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 11 Cze 2019, 18:27
-- Wersja serwera: 10.1.38-MariaDB
-- Wersja PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `filmy`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `film`
--

CREATE TABLE `film` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '5',
  `tytul` text COLLATE utf8_polish_ci NOT NULL,
  `opis` text COLLATE utf8_polish_ci NOT NULL,
  `obsada` text COLLATE utf8_polish_ci NOT NULL,
  `gatunek` text COLLATE utf8_polish_ci NOT NULL,
  `zdj` text COLLATE utf8_polish_ci NOT NULL,
  `like` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `film`
--

INSERT INTO `film` (`id`, `id_user`, `tytul`, `opis`, `obsada`, `gatunek`, `zdj`, `like`) VALUES
(1, 5, 'Narodziny gwiazdy', 'Piekielnie zdolna, choć niepewna siebie Ally (Lady Gaga) przypadkiem spotyka w barze gwiazdora country (Bradley Cooper). Mimo różnic zakochują się w sobie i postanawiają wyruszyć we wspólną trasę. Dla niej to uczucie stanie się inspiracją do artystycznej pracy, a także katapultą do wielkiej kariery. Dla niego - być może początkiem końca.', 'Lady Gaga, Bradley Cooper, Sam Elliott, Dave Chappelle, Anthony Ramos', 'Dramat', 'https://nahatymanaty.cz/public/GIF/FILMY/star-is-born-cooper-gaga.gif', 0),
(2, 5, 'Zanim się pojawiłeś', 'Zanim się pojawiłeś – dramat produkcji amerykańskiej i brytyjskiej, na podstawie powieści Jojo\r\n                Moyes. Lou Clark jest młoda, energiczna i „wszystkowiedząca”. Ma chłopaka, którego nie kocha i pracę,\r\n                którą lubi. Niespodziewanie zostaje zwolniona. Zatrudnia się jako opiekunka bogatego, niepełnosprawnego\r\n                Willa, którego życie legło w gruzach po tragicznym wypadku sprzed kilkunastu miesięcy. Miłość do\r\n                szybkiej jazdy na motorze przypłacił swoim szczęściem i spokojnym życiem. Uważa, że już nie ma po co\r\n                żyć, wszystko straciło dla niego kolory. Jednak spotkanie z Lou wprowadziło do jego życia powiew\r\n                radości, nadziei, którą już całkowicie stracił.\r\n     ', 'Emilia Clarke, Sam Claflin, Janet McTeer,Charles Dance, Brendan Coyle, Stephen Peacocke', 'Dramat', 'https://2.bp.blogspot.com/-Zk9oBfA1X6M/V2Zw53NsMEI/AAAAAAAAApE/JinU1fEEmF0tCkk3fHzTuadELx8WDSitgCLcB/s1600/tumblr_o6gwaqrIUw1r9uuz0o2_500.gif', 4),
(4, 5, 'Kac Vegas', 'Kac Vegas– film komediowy produkcji amerykańsko-niemieckiej z 2009 r. w reżyserii Todda\r\n                Phillipsa. Obraz otrzymał Złoty Glob dla Najlepszego filmu komediowego lub musicalu.\r\n', 'Bradley Cooper, Justin Bartha, Ed Helms, Zach Galifianakis, Heather Graham', 'Komedia', 'https://i1.kwejk.pl/k/obrazki/2012/07/91fdad17b83e451228b3a13aa26297db.gif', 0),
(5, 5, 'Millerowie ', 'Millerowie – amerykańska komedia z 2013 roku w reżyserii Rawsona Marshalla Thurbera. Wyprodukowana przez Warner Bros. Światowa premiera filmu miała miejsce 7 sierpnia 2013 roku. W Polsce premiera filmu    odbyła się 15 sierpnia 2013 roku.\r\n      ', 'Jason Sudeikis,Jennifer Aniston, Will Poulter, Emma Roberts', 'Komedia', 'http://25.media.tumblr.com/aff9ff6be049bc2f9e09d71aa9090681/tumblr_mnmd4dzaeA1sri6gpo1_500.gif', 2),
(6, 5, 'Wonder Woman', 'Zanim stała się Wonder Woman, była Dianą, księżniczką Amazonek wyszkoloną na niepokonaną wojowniczkę. Wychowała się na odległej, rajskiej wyspie. Pewnego dnia rozbił się tam amerykański pilot, który opowiedział Dianie o wielkim konflikcie ogarniającym świat. Księżniczka porzuciła więc swój dom przekonana, że może powstrzymać zagrożenie. W walce u boku ludzi, w wojnie ostatecznej, Diana odkrywa pełnię swojej mocy... i swoje prawdziwe przeznaczenie.', 'Gal Gadot, Chris Pine, Robin Wright', 'Akcja', 'https://i.redd.it/e2p37t9avm101.gif', 0),
(7, 5, 'Szybcy i wściekli 8', 'Dom i Letty cieszą się swym miesiącem miodowym, Brian i Mia wycofali się z gry, a reszcie ekipy udało się znaleźć oczyszczającą namiastkę normalnego życia. To właśnie wtedy pojawia się ona. Tajemnicza kobieta (Charlize Theron), która wciąga Doma w niebezpieczny świat, z którego nie ma ucieczki. Dom zdradzi najbliższych. Wszystkich czeka czas pełen prób i testów, jakich nie widzieliśmy dotąd w kinie. Od wybrzeży Kuby przez ulice Nowego Jorku po lody arktycznego morza Barentsa – „Szybcy i wściekli” przemierzą świat, by powstrzymać zło i chaos. I uratować tego, który uczynił ich rodziną. ', 'Vin Diesel, Dwayne Johnson, Jason Statham', 'Akcja', 'https://madridfree.org/wp-content/uploads/2015/06/fast7.gif', 8),
(9, 5, 'Love, Rosie', 'Rosie i Alex znają się od dzieciństwa. Gdy Alex wyjeżdża z Dublina na studia do Ameryki, ich wieloletnią przyjaźń czekają ciężkie chwile. Czy znajomość przetrwa lata rozłąki i tysiące kilometrów? Czy zaryzykują wszystko dla rodzącej się między nimi miłości? ', 'Lily Collins, Sam Claflin, Christian Cooke', 'Romans', 'https://3.bp.blogspot.com/-ZajOnyuT_Uo/Vu1tMaUMGKI/AAAAAAAABmY/RwKomhI1acsXu3rrVu_wWT__xGiaQV7zA/s640/lily-love-rosie.gif', 0),
(10, 5, 'Iron Man', 'Miliarder Tony Stark (Robert Downey Jr.), jeden z najbardziej pomysłowych wynalazców technologii wojennej służącej do obrony, postanawia odsunąć się od produkcji broni. Potężny konkurent próbuje zabić Starka i prawie mu się to udaje. Aby utrzymać się przy życiu, miliarder postanawia wyprodukować nowoczesny kostium-zbroję. Z czasem zaczyna budować coraz to nowsze, bardziej potężne i rozbudowane wersje kostiumu i zaczyna je używać do zwalczania przestępczości jako niezwyciężony Iron Man.', 'Robert Downey Jr., Terrence Howard, Jeff Bridges, Shaun Toub, Gwyneth Paltrow', 'Akcja', 'https://thumbs.gfycat.com/AgileBaggyHackee-size_restricted.gif', 0),
(11, 5, 'Szybcy i wściekli', 'Opowieść o gangach, które rywalizują o prymat, ścigając się na ulicach miast przerobionymi samochodami, najczęściej produkcji japońskiej. Są to wyścigi o duże pieniądze. Gangi podejrzewane są także o serię porwań ciężarówek. Aby zlikwidować ten proceder, do jednej z grup przenika policjant, Brian (Paul Walker). Po pewnym czasie przekonuje do siebie szefa Dominica Toretto (Vin Diesel). Rywalizacja między gangami przybiera na sile.', 'Vin Diesel, Paul Walker, Michelle Rodriguez, Jordana Brewster', 'Akcja', 'https://img-ovh-cloud.zszywka.pl/0/0271/1767-szybcy-i-wsciekli-lt3.gif', 6),
(12, 5, 'Kill Bill', 'Panna Młoda (Uma Thurman), należąca niegdyś do płatnych zabójców, a która obecnie spodziewa się dziecka, zostaje zdradzona w dniu swojego ślubu przez swojego szefa (David Carradine). Cudem unika śmierci, mimo iż została postrzelona w głowę. Znajduje się jednak w śpiączce. Po czterech latach budzi się ze śpiączki i postanawia krwawo zemścić się na każdym, kto miał związek z zamachem na jej życie, pozostawiając sobie zemstę na tytułowym Billu na koniec.', 'Uma Thurman,David Carradine, Vivica A. Fox\r\nLucy Liu, Michael Madsen, Daryl Hannah, Sonny Chiba', 'Akcja', 'https://media1.giphy.com/media/M7ldoZBJ2WrK/giphy.gif', 0),
(13, 5, '21 Jump Street', 'Kapitalna komedia o młodych policjantach, których zadaniem będzie wykrycie siatki dealerów narkotyków w liceum. Film pełen zabawnych sytuacji i dowcipnych dialogów.   ', 'Jonah Hill, Channing Tatum, Brie Larson, Dave Franco, Rob Riggle ', 'Komedia', 'https://media.giphy.com/media/JX2ecp7kW9G12/giphy.gif', 0),
(14, 5, 'Kevin sam w domu', 'Rodzina McCallisterów zamierza spędzić Święta Bożego Narodzenia we Francji. W dzień wyjazdu omal nie spóźniają się na samolot. W wyniku małego zamieszania zapominają ze sobą zabrać Kevina (Macaulay Culkin). Ośmioletni chłopiec zostaje sam w domu, od tej pory musi sam sobie radzić ze wszystkim, w czym do tej pory wyręczali go rodzice... łącznie z dwoma złodziejami (Joe Pesci, Daniel Stern), którzy tylko czyhają, by okraść dom McCallisterów. ', 'Macaulay Culkin, Joe Pesci, Daniel Stern, Roberts Blossom', 'Komedia', 'http://faner.pl/wp-content/uploads/2014/12/fakty-o-kevin-sam-w-domu20.gif', 7),
(15, 5, 'Gwiazd naszych wina', 'Hazel (Shailene Woodley) i Augustus (Ansel Elgort) to nastolatkowie. Oboje są dowcipni, inteligentni, ciekawi świata i zmagający się z chorobą nowotworową.  On jest w remisji, ona nie rusza się nigdzie bez zbiornika z tlenem. Poznają się przypadkowo podczas spotkania grupy wsparcia. Na przekór sytuacji, w której się znajdują, zakochują się w sobie i uczą nawzajem czerpać radość z życia. ', 'Shailene Woodley, Ansel Elgort, Nat Wolff, Laura Dern, Laura Dern', 'Dramat', 'https://em.wattpad.com/acd01eda5af2a810748c80043a03a5ff2ee6e0d1/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f776174747061642d6d656469612d736572766963652f53746f7279496d6167652f325864776861685a563834546a673d3d2d3235392e313533633637346131616532393166643832393433363937343431332e676966', 0),
(16, 5, 'Szkoła uczuć', 'Landon (Shane West) jest najpopularniejszym chłopcem w szkole. Jest zabawny, ma super ciuchy, najładniejsze panny i fajowych kumpli. Jest po prostu cool. Pewnego dnia postanawia wraz z kumplami trochę ponabijać się z pierwszoklasisty (Matt Lutz), co kończy się tragedią. W ramach resocjalizacji zostaje przydzielony przez dyrektora szkoły do \"załogi sprzątającej\" korytarz, w soboty musi pomagać w lekcjach matołkom z podstawówki, i o zgrozo, wystąpić w kwaśnej sztuce, przygotowywanej przez kółko teatralne na koniec roku szkolnego. Tragedia. Wolałby łyżeczką zdrapywać gumę do żucia z autostrady niż dać się tak publicznie upokorzyć. Tym czasem okazuje się, że we wszystkich tych smętnych zajęciach uczestniczy, i to zupełnie po dobroci, Jamie (Mandy Moore) - rzewna córka miejscowego pastora (Peter Coyote). Ubrana w workowate kiece Jamie śpiewa również w kościelnym chórze i ogląda gwiazdy przez teleskop własnej produkcji. Jamie jest wyszydzanym kozłem ofiarnym grupy Landona, ma jednak dobre serce i kiedy ten prosi ją o pomoc w opanowaniu roli, zgadza się bez wahania. Stawia tylko jeden warunek. Chłopak musi obiecać, że się w niej nie zakocha. Nic prostszego? Wręcz przeciwnie.', 'Shane West, Mandy Moore, Al Thompson, Peter Coyote, Daryl Hannah', 'Dramat', 'https://img-ovh-cloud.zszywka.pl/0/0313/5593-ns-quotszkola-uczucquot.gif', 0),
(17, 5, 'Wciąż ją kocham', '„Dear John” to kolejna adaptacja książki Nicholasa Sparksa. Opowiada historię 23-letniego chłopaka Johna Tyrce (Channing Tatum), który zaprzyjaźnia się z nowo poznaną dziewczyną Savannah ( Amanda Seyfried ). Wkrótce ich przyjaźń przeradza się w głębsze uczucie, jednakże na skutek napiętych relacji z ojcem, John wstępuje do armii amerykańskiej. Czy wiążące ich uczucie ma szansę przetrwać próbę czasu? „Dear John” to romantyczna opowieść o miłości, poświęceniu i przeciwnościach losu. ', 'Amanda Seyfried, Channing Tatum, Richard Jenkins, Scott Porter', 'Dramat', 'https://img-ovh-cloud.zszywka.pl/0/0311/6698-quotwciaz-ja-kochamquot.gif', 0),
(18, 5, 'La La Land ', 'Mia jest początkującą aktorką, która w oczekiwaniu na szansę pracuje jako kelnerka. Sebastian to muzyk jazzowy, który zamiast nagrywać płyty, gra do kotleta w podrzędnej knajpce. Gdy drogi tych dwojga przetną się, połączy ich wspólne pragnienie, by zacząć wreszcie robić to co kochają. Miłość dodaje im sił, ale gdy kariery zaczynają się wreszcie układać, coraz mniej jest czasu i sił dla siebie nawzajem. Czy uda im się ocalić uczucie, nie rezygnując z marzeń?', 'Ryan Gosling, Emma Stone, John Legend', 'Romans', 'https://media0.giphy.com/media/l3vRaHTeHMH0fQ9fG/giphy.gif', 4),
(19, 5, 'Holiday', 'Iris (Kate Winslet) jest zakochana w mężczyźnie, który właśnie ma poślubić inną. Żyjąca po drugiej stronie kuli ziemskiej, Amanda (Cameron Diaz), dowiaduje się, że człowiek, z którym mieszka był jej niewierny. Dwie kobiety, które nigdy się nie spotkały i mieszkają w odległości ponad 4 000 kilometrów, spotykają się w sieci, na stronie pomagającej aranżować zamianę domów i pod wpływem impulsu wymieniają się miejscami zamieszkania na okres świąt. Iris wprowadza się do domu Amandy w słonecznym Los Angeles, a Amanda przyjeżdża na pokrytą śniegiem angielską wieś. Wkrótce po przybyciu na miejsce przeznaczenia, obie kobiety odnajdują to, czego najmniej się spodziewały: romans. Amanda jest oczarowana przystojnym bratem Iris, Grahamem (Jude Law), natomiast Iris, wiedziona inspiracją legendarnego hollywoodzkiego scenarzysty Arthura (Eli Wallach), leczy swoje złamane serce spotykając się z kompozytorem muzyki filmowej, Milesem (Jack Black).', 'Kate Winslet, Cameron Diaz, Jude Law, Eli Wallach', 'Romans', 'http://www.celebquote.com/wp-content/uploads/2013/01/the-holiday-movie-quotes-15.gif', 0),
(20, 5, 'Pretty Woman', 'Zabawny i poruszający film Garry\'ego Marshalla stał się wydarzeniem sezonu i wielkim przebojem kasowym na całym świecie, przynosząc producentom milionowe zyski. To historia nieokrzesanej panienki lekkich obyczajów, którą zmusił do wyjścia na ulicę brak pieniędzy na komorne. Dzięki swej żywiołowości i naturalności, Vivian Ward zdoła podbić serce przystojnego milionera i na zawsze odmieni swoje życie. Spotyka go właśnie na ulicy, w Los Angeles, dokąd przyjechała z małego miasteczka.', 'Julia Roberts, Richard Gere, Héctor Elizondo, Laura San Giacomo', 'Romans', 'https://66.media.tumblr.com/540ed25887ef271ce85535f621ef2115/tumblr_onf75t7c3h1w035b6o2_500.gif', 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ulubione`
--

CREATE TABLE `ulubione` (
  `id_fav` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_movie` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ulubione`
--

INSERT INTO `ulubione` (`id_fav`, `id_user`, `id_movie`) VALUES
(7, 5, 9),
(8, 5, 5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownicy`
--

CREATE TABLE `uzytkownicy` (
  `id` int(11) NOT NULL,
  `username` text COLLATE utf8_polish_ci NOT NULL,
  `password` text COLLATE utf8_polish_ci NOT NULL,
  `email` text COLLATE utf8_polish_ci NOT NULL,
  `photo` text COLLATE utf8_polish_ci NOT NULL,
  `gender` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `uzytkownicy`
--

INSERT INTO `uzytkownicy` (`id`, `username`, `password`, `email`, `photo`, `gender`) VALUES
(5, 'magda123', 'magda123', 'magdalena.kedzia3@gmail.com', 'myImage-1558380658045.jpg', 'Kobieta'),
(74, 'magda1233', 'magda123', 'magda123@magda.com', 'myImage-1558382600071.png', 'Kobieta'),
(75, '123', '123', 'on123@gmail.com', 'myImage-1558454784389.png', 'Mężczyzna'),
(77, 'marek12', 'marek12', 'marek12@mare.com', 'myImage-1558455575272.png', 'Mężczyzna'),
(78, 'on123', '123', 'magdalena.kedzia3@gmail.com', 'myImage-1558457984520.png', 'Mężczyzna'),
(79, 'magda123x', 'magda123x', 'magda123@magda.com', 'myImage-1559588752240.png', 'Kobieta'),
(80, 'magda123', 'magda123', 'magdalena.kedzia3@gmail.com', 'myImage-1559713761718.png', 'Kobieta'),
(81, 'magda123', 'magda123', 'magda123@magda.com', 'myImage-1559713832370.png', 'Kobieta'),
(82, 'ania1234', '123', 'ania@ana.com', 'myImage-1559726404119.png', 'Kobieta');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `ulubione`
--
ALTER TABLE `ulubione`
  ADD PRIMARY KEY (`id_fav`);

--
-- Indeksy dla tabeli `uzytkownicy`
--
ALTER TABLE `uzytkownicy`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `film`
--
ALTER TABLE `film`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT dla tabeli `ulubione`
--
ALTER TABLE `ulubione`
  MODIFY `id_fav` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `uzytkownicy`
--
ALTER TABLE `uzytkownicy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
