// builtin
var fs = require('fs');

// 3rd party
var express = require('express');
var hbs = require('hbs');
const mysql = require('mysql');
var app = express();
var session = require('express-session');
var async = require('async');
const multer = require('multer');
const path = require('path');
const moment = require('moment');


moment.locale('pl');
const conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'filmy'
});
app.use(session({
    secret: '2C44-4D44-WppQ38S',
    resave: true,
    saveUninitialized: true
}));
const storage = multer.diskStorage({
    destination: './public/uploads/',
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

// Init Upload
const uploads = multer({
    storage: storage,
    limits: { fileSize: 1000000 },
    fileFilter: function (req, file, cb) {
        checkFileType(file, cb);
    }
}).single('myImage');
// Check File Type
function checkFileType(file, cb) {
    // Allowed ext
    const filetypes = /jpeg|jpg|png|gif/;
    // Check ext
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    // Check mime
    const mimetype = filetypes.test(file.mimetype);

    if (mimetype && extname) {
        return cb(null, true);
    } else {
        cb('Error: Images Only!');
    }
}
const parser = multer({ storage: storage });
var auth = function (req, res, next) {
    if (req.session && req.session.user === "user" && req.session.admin)
        return next();
    else
        return res.redirect('/');

};
hbs.registerPartial('partial', fs.readFileSync(__dirname + '/views/partials/partial1.hbs', 'utf8'));
hbs.registerPartials(__dirname + '/views/partials');

hbs.registerHelper('ifvalue', function (conditional, options) {
    if (options.hash.value === conditional) {
        return options.fn(this)
    } else {
        return options.inverse(this);
    }
});

// set the view engine to use handlebars
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

conn.connect((err) => {
    console.log('Mysql Connected...');
});

app.use(express.static(__dirname + '/public'));


app.get('/', function (req, res) {
    let sql = "SELECT * FROM film";
    let query = conn.query(sql, (err, results) => {

        res.render('index', {
            results: results,
            nameOfUser: global.id_username
        });
    });
});

app.get('/category', function (req, res) {

    let sql = "SELECT * FROM film, uzytkownicy where gatunek = '" + req.query.category + "' and film.id_user  = uzytkownicy.id;";

    let query = conn.query(sql, (err, results) => {
        res.render('category', {
            category: req.query.category,
            results: results,
            nameOfUser: global.id_username
        });
    });
});
app.get('/contact', function (req, res) {


    let query = conn.query((err, results) => {
        res.render('contact', { nameOfUser: global.id_username });
    });
});
app.get('/openRegister', (req, res) => {
    res.render('register');
});
app.get('/openProfile', (req, res) => {

    async.series({
        user: function (cb) {
            conn.query("select * from uzytkownicy where id='" + global.id_user + "';", function (error, result, client) {
                cb(error, result);
            })
        },
        movie: function (cb) {
            conn.query("SELECT * from  film where id_user=" + global.id_user + ";", function (error, result, client) {
                cb(error, result)
            })
        }
    },
        function (error, results) {
            let mes;
            if (results.movie[0] == undefined) { mes = "Brak" }

            res.render('profile', {
                user: results.user,
                movie: results.movie,
                mes:mes,
                nameOfUser: global.id_username
            });
        });
});

app.post('/register', parser.single("myImage"), (req, res) => {
    // console.log(req.file)
    console.log(req.session.user)
    fs.writeFile(`uploads/${req.file.filename}`, 'binary', function (err) { });

    let data = { username: req.body.username, password: req.body.password, email: req.body.email, photo: req.file.filename, gender: req.body.gender };
    let sql = "INSERT INTO uzytkownicy SET ?";
    let query = conn.query(sql, data, (err, results) => {


        global.id_username = data.username;
        req.session.user = "user";
        req.session.admin = true;
        let arr = [];
        arr[0] = data;
        res.render('profile', { user: arr, nameOfUser: global.id_username, mes:"brak", });
    })

})

app.get('/login', function (req, res) {
    var username = req.query.username;
    var password = req.query.password;
    if (username && password) {
        conn.query('SELECT * FROM uzytkownicy WHERE username = ? AND password = ?', [username, password], function (error, results, fields) {
            if (results.length > 0) {
                global.id_user = results[0].id;
                global.id_username = results[0].username;
                req.session.user = "user";
                req.session.admin = true;
                res.redirect('/');
            }
        })
    }
});

app.get('/logout', function (req, res) {
    global.id_user = undefined;
    global.id_username = undefined;
    req.session.destroy();
    res.redirect('/');
});

app.get('/addFav', (req, res) => {
  
    let sql = "INSERT INTO ulubione SET ?";
    let data = { id_user: global.id_user, id_movie: req.query.id };
    let query = conn.query(sql, data, (err, results) => {
        res.render('index', {  nameOfUser: global.id_username});
      
    });

});
app.get('/delFav', (req, res) => {
  
    let sql = "DELETE FROM ulubione where  ulubione.id_fav = "+ req.query.id +" ;";
    let query = conn.query(sql, (err, results) => {
        res.render('index', {  nameOfUser: global.id_username});
      
    });
});

app.get('/openFav', (req, res) => {

    let sql = "select * from ulubione, uzytkownicy, film where  ulubione.id_user = "+ global.id_user +" and ulubione.id_movie = film.id and ulubione.id_user  = uzytkownicy.id;";
    let query = conn.query(sql,  (err, results) => {
        res.render('favourite', {results:results, nameOfUser: global.id_username});     
    });
});

app.listen(3000);

