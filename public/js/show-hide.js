$(document).ready(function () {

  var i = 0;

  $("#flip").click(function () {
    i++;
    $("#winners").slideToggle("slow");
    if (i % 2 == 0) {
      $(".modal").toggleClass('is-visible');
    }
  });

  $(".modalClose").click(function () {
    $('.modal').toggleClass('is-no-visible');
  });

});